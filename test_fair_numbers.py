from pretty_numbers import nicer
from random import randint


def test_on_format_thousands():
    for i in range(1000):
        assert len(nicer(randint(1000, 9999))) == 5
    for i in range(1000):
        assert len(nicer(randint(10000, 99999))) == 6
    for i in range(1000):
        assert len(nicer(randint(100000, 999999))) == 7


def test_on_format_millions():
    for i in range(1000):
        assert len(nicer(randint(1000000, 9999999))) == 9
    for i in range(1000):
        assert len(nicer(randint(10000000, 99999999))) == 10
    for i in range(1000):
        assert len(nicer(randint(100000000, 999999999))) == 11


def test_on_format_billions():
    for i in range(1000):
        assert len(nicer(randint(1000000000, 9999999999))) == 13
    for i in range(1000):
        assert len(nicer(randint(10000000000, 99999999999))) == 14
    for i in range(1000):
        assert len(nicer(randint(100000000000, 999999999999))) == 15
